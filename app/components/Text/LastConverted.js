import React from 'react';
import PropTypes from 'prop-types';

import { Text } from 'react-native';

import styles from './styles';

const LastConverted = ({
  base, quote, conversionRate, date,
}) => (
  <Text style={styles.smallText}>
    {base} = {conversionRate} {quote} as of { date }
  </Text>
);

LastConverted.propTypes = {
  base: PropTypes.string,
  quote: PropTypes.string,
  conversionRate: PropTypes.number,
  date: PropTypes.string,
};

export default LastConverted;
